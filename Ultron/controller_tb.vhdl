library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of controller_tb is
component controller is
   port(clk		:in    std_logic;
	reset	:in    std_logic;
	left    :in    std_logic;
        right   :in    std_logic;
        quadrant:inout   std_logic_vector(1 downto 0);
        step    :inout   std_logic_vector(3 downto 0));
end component;
signal clk: std_logic;
signal reset: std_logic;
signal left: std_logic;
signal right: std_logic;
signal quadrant: std_logic_vector(1 downto 0);
signal step: std_logic_vector(3 downto 0);
begin
	lbl1: controller port map(clk, reset, left, right, quadrant, step);
		clk <= '1' after 0 ns, '0' after 1 ns when clk /= '0' else '1' after 1 ns;
		reset <= '0', '1' after 40 ns, '0' after 230 ns;
		left <= '0', '1' after 20 ms, '0' after 3200 ms; --, '1' after 1300 ns, '0' after 5300 ns;
		right <= '0', '1' after 3250 ms, '0' after 5900 ms; --, '1' after 5500 ns, '0' after 8000 ns;
end behaviour;