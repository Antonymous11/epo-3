library IEEE;
use IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;

architecture behaviour of decoder is
begin
P1:process(clk, reset)
begin

if clk'event and clk = '1' then
	if (reset = '1') then
		x0 <= "0000000";
		x1 <= "0000000";
		height <= "000000";
	else
	height <= "001000";
		case block_type is
			when "000" => x0 <= "0000000"; x1 <= "0101101";
			when "100" => x0 <= "0000000"; x1 <= "0101101";
			when "001" => x0 <= "0100011"; x1 <= "1010000";
			when "101" => x0 <= "0100011"; x1 <= "1010000";
			when "110" => x0 <= "0011101"; x1 <= "0110011";
			when "010" => x0 <= "0011101"; x1 <= "0110011";
			when "111" => x0 <= "0000000"; x1 <= "1010000";
			when "011" => x0 <= "0000000"; x1 <= "1010000";
			when others => x0 <= "0000000"; x1 <= "0000000";
		end case;
	end if;
end if;
end process;
end behaviour;