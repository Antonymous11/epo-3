configuration ultron_behaviour_cfg of ultron is
   for behaviour
      for all: controller use configuration work.controller_behaviour_cfg;
      end for;
      for all: coordcalc use configuration work.coordcalc_behaviour_cfg;
      end for;
   end for;
end ultron_behaviour_cfg;


