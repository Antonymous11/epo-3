library IEEE;
use IEEE.std_logic_1164.all;

architecture behaviour of vga_output is
begin
output : process (color, rom_mux_output)
begin
	case color is
		when "01" => 
			vga_red <= rom_mux_output;
			vga_green <= '0';
			vga_blue <= '0';
		when "10" => 
			vga_blue <= rom_mux_output;
			vga_green <= '0';
			vga_red <= '0';
		when "00" => 
			vga_red <= rom_mux_output;
			vga_green <= rom_mux_output;
			vga_blue <= rom_mux_output;
		when others => 
			vga_red <= '0';
			vga_green <= '0';
			vga_blue <= '0';
	end case;
end process;

end behaviour;