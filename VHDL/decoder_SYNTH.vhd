
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of decoder is

   component no210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component na210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component dfn10
      port( D, CK : in std_logic;  Q : out std_logic);
   end component;
   
   component dfr11
      port( D, R, CK : in std_logic;  Q : out std_logic);
   end component;
   
   signal N20, N28, N38, N39, N40, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11
      , n13, n14, n23, n24, n25, n26, n27, n28_port, n29, n30, n31 : std_logic;

begin
   
   height_reg_3_inst : dfr11 port map( D => n9, R => reset, CK => clk, Q => 
                           height(3));
   x0_reg_5_inst : dfr11 port map( D => n11, R => n10, CK => clk, Q => x0(5));
   x0_reg_4_inst : dfr11 port map( D => n13, R => n10, CK => clk, Q => x0(4));
   x0_reg_3_inst : dfr11 port map( D => n13, R => n10, CK => clk, Q => x0(3));
   x0_reg_2_inst : dfr11 port map( D => n13, R => n10, CK => clk, Q => x0(2));
   x0_reg_1_inst : dfr11 port map( D => n11, R => n10, CK => clk, Q => x0(1));
   x0_reg_0_inst : dfr11 port map( D => N28, R => n10, CK => clk, Q => x0(0));
   x1_reg_6_inst : dfr11 port map( D => N40, R => n10, CK => clk, Q => x1(6));
   x1_reg_5_inst : dfn10 port map( D => n8, CK => clk, Q => x1(5));
   x1_reg_4_inst : dfr11 port map( D => N38, R => n10, CK => clk, Q => x1(4));
   x1_reg_3_inst : dfn10 port map( D => n6, CK => clk, Q => x1(3));
   x1_reg_2_inst : dfn10 port map( D => n4, CK => clk, Q => x1(2));
   x1_reg_1_inst : dfr11 port map( D => n13, R => n10, CK => clk, Q => x1(1));
   x1_reg_0_inst : dfn10 port map( D => n2, CK => clk, Q => x1(0));
   U3 : no210 port map( A => reset, B => n1, Y => n2);
   U4 : no210 port map( A => N20, B => N39, Y => n1);
   U5 : no210 port map( A => reset, B => n3, Y => n4);
   U6 : no210 port map( A => N20, B => n14, Y => n3);
   U7 : no210 port map( A => reset, B => n5, Y => n6);
   U8 : no210 port map( A => N20, B => n14, Y => n5);
   U9 : no210 port map( A => reset, B => n7, Y => n8);
   U10 : no210 port map( A => N20, B => N39, Y => n7);
   x0(6) <= '0';
   height(0) <= '0';
   height(1) <= '0';
   height(2) <= '0';
   n9 <= '1';
   height(4) <= '0';
   height(5) <= '0';
   U36 : iv110 port map( A => n23, Y => n13);
   U37 : iv110 port map( A => n24, Y => n11);
   U38 : iv110 port map( A => n25, Y => n10);
   U39 : no210 port map( A => reset, B => N20, Y => n25);
   U40 : na210 port map( A => n26, B => n23, Y => N39);
   U41 : na210 port map( A => n23, B => n27, Y => N38);
   U42 : iv110 port map( A => N40, Y => n27);
   U43 : na210 port map( A => n28_port, B => n24, Y => N40);
   U44 : na210 port map( A => n23, B => n24, Y => N28);
   U45 : na210 port map( A => n29, B => n30, Y => n24);
   U46 : na210 port map( A => block_type(2), B => n28_port, Y => n30);
   U47 : na210 port map( A => n28_port, B => n31, Y => n23);
   U48 : na210 port map( A => block_type(2), B => n29, Y => n31);
   U49 : iv110 port map( A => block_type(1), Y => n29);
   U50 : iv110 port map( A => block_type(0), Y => n28_port);
   U51 : no210 port map( A => n26, B => block_type(2), Y => N20);
   U52 : iv110 port map( A => n14, Y => n26);
   U53 : no210 port map( A => block_type(0), B => block_type(1), Y => n14);

end synthesised;



