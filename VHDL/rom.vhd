library ieee;
use ieee.std_logic_1164.all;

entity rom is
  port ( address : in std_logic_vector(4 downto 0);
         q : out std_logic_vector(7 downto 0) ); 
end entity rom;

