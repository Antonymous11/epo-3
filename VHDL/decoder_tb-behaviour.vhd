library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of decoder_tb is
component decoder is
   port(clk			:in std_logic;
	reset		:in std_logic;
	block_type		:in    std_logic_vector(2 downto 0);
	x0		:out   std_logic_vector(6 downto 0);
	x1		:out   std_logic_vector(6 downto 0);
	height		:out   std_logic_vector(5 downto 0));
end component;
signal clk, reset: std_logic;
signal block_type: std_logic_vector(2 downto 0);
signal x0, x1: std_logic_vector(6 downto 0);
signal height: std_logic_vector(5 downto 0);
begin
	lbl1: decoder port map(clk, reset, block_type, x0, x1, height);
		clk <= '1' after 0 ns, '0' after 1 ns when clk /= '0' else '1' after 1 ns;
		reset <= '1', '0' after 3 ns;
		block_type <= "000" after 10 ns, "101" after 20 ns, "010" after 30 ns, "111" after 40 ns, "100" after 50 ns, "110" after 60 ns, "001" after 70 ns, "011" after 80 ns;
end behaviour;


