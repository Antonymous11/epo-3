
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of coordcalc is

   component no310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component no210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component ex210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component mu111
      port( A, B, S : in std_logic;  Y : out std_logic);
   end component;
   
   component na310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component dfr11
      port( D, R, CK : in std_logic;  Q : out std_logic);
   end component;
   
   component dfn10
      port( D, CK : in std_logic;  Q : out std_logic);
   end component;
   
   signal N41, N51, N52, N53, N54, N55, N56, N57, N58, N59, N60, N77, N78, N79,
      N80, N91, N92, N93, N94, N97, N98, N99, N100, N101, N122, N123, N124, 
      N125, N138, N139, N140, N141, N145, N146, N147, N148, N149, n13, n14, n15
      , n16, n17, n18, n112, n113, n114, n115, n116, n117, n118, n119, n120, 
      n121, n122_port, n123_port, n124_port, n125_port, n126, n127, n128, n129,
      n130, n131, n132, n133, n134, n135, n136, n137, n138_port, n139_port, 
      n140_port, n141_port, n142, n143, n144, n145_port, n146_port, n147_port, 
      n148_port, n149_port, n150, n151, n152, n153, n154, n155, n156, n157, 
      n158, n159, n160, n161, n162, n163, n164, n165, n166, n167, n168, n169, 
      n170, n171, n172 : std_logic;

begin
   
   sinarrayY_reg_4_inst : dfr11 port map( D => N60, R => n168, CK => clk, Q => 
                           N125);
   sinarrayY_reg_3_inst : dfr11 port map( D => N59, R => n168, CK => clk, Q => 
                           N124);
   sinarrayY_reg_2_inst : dfr11 port map( D => N58, R => n168, CK => clk, Q => 
                           N123);
   sinarrayY_reg_1_inst : dfr11 port map( D => N57, R => n168, CK => clk, Q => 
                           N122);
   sinarrayY_reg_0_inst : dfr11 port map( D => N56, R => n168, CK => clk, Q => 
                           N145);
   y_red_reg_7_inst : dfr11 port map( D => n169, R => reset, CK => clk, Q => 
                           y_red(7));
   y_red_reg_6_inst : dfr11 port map( D => n169, R => reset, CK => clk, Q => 
                           y_red(6));
   y_red_reg_5_inst : dfr11 port map( D => n169, R => reset, CK => clk, Q => 
                           y_red(5));
   y_red_reg_4_inst : dfr11 port map( D => N141, R => reset, CK => clk, Q => 
                           y_red(4));
   y_red_reg_3_inst : dfr11 port map( D => N140, R => reset, CK => clk, Q => 
                           y_red(3));
   y_red_reg_2_inst : dfr11 port map( D => N139, R => reset, CK => clk, Q => 
                           y_red(2));
   y_red_reg_1_inst : dfr11 port map( D => N138, R => reset, CK => clk, Q => 
                           y_red(1));
   y_red_reg_0_inst : dfr11 port map( D => N145, R => reset, CK => clk, Q => 
                           y_red(0));
   y_blue_reg_7_inst : dfr11 port map( D => n170, R => reset, CK => clk, Q => 
                           y_blue(7));
   y_blue_reg_6_inst : dfr11 port map( D => n170, R => reset, CK => clk, Q => 
                           y_blue(6));
   y_blue_reg_5_inst : dfr11 port map( D => n170, R => reset, CK => clk, Q => 
                           y_blue(5));
   y_blue_reg_4_inst : dfr11 port map( D => N149, R => reset, CK => clk, Q => 
                           y_blue(4));
   y_blue_reg_3_inst : dfr11 port map( D => N148, R => reset, CK => clk, Q => 
                           y_blue(3));
   y_blue_reg_2_inst : dfr11 port map( D => N147, R => reset, CK => clk, Q => 
                           y_blue(2));
   y_blue_reg_1_inst : dfr11 port map( D => N146, R => reset, CK => clk, Q => 
                           y_blue(1));
   y_blue_reg_0_inst : dfr11 port map( D => N145, R => reset, CK => clk, Q => 
                           y_blue(0));
   sinarray_reg_4_inst : dfn10 port map( D => n18, CK => clk, Q => N80);
   sinarray_reg_3_inst : dfr11 port map( D => N54, R => n168, CK => clk, Q => 
                           N79);
   sinarray_reg_2_inst : dfr11 port map( D => N53, R => n168, CK => clk, Q => 
                           N78);
   sinarray_reg_1_inst : dfn10 port map( D => n16, CK => clk, Q => N77);
   sinarray_reg_0_inst : dfn10 port map( D => n14, CK => clk, Q => N97);
   x_red_reg_0_inst : dfr11 port map( D => N97, R => reset, CK => clk, Q => 
                           x_red(0));
   x_blue_reg_0_inst : dfr11 port map( D => N97, R => reset, CK => clk, Q => 
                           x_blue(0));
   x_red_reg_1_inst : dfr11 port map( D => N91, R => reset, CK => clk, Q => 
                           x_red(1));
   x_blue_reg_1_inst : dfr11 port map( D => N98, R => reset, CK => clk, Q => 
                           x_blue(1));
   x_red_reg_2_inst : dfr11 port map( D => N92, R => reset, CK => clk, Q => 
                           x_red(2));
   x_blue_reg_2_inst : dfr11 port map( D => N99, R => reset, CK => clk, Q => 
                           x_blue(2));
   x_blue_reg_3_inst : dfr11 port map( D => N100, R => reset, CK => clk, Q => 
                           x_blue(3));
   x_red_reg_3_inst : dfr11 port map( D => N93, R => reset, CK => clk, Q => 
                           x_red(3));
   x_blue_reg_4_inst : dfr11 port map( D => N101, R => reset, CK => clk, Q => 
                           x_blue(4));
   x_red_reg_4_inst : dfr11 port map( D => N94, R => reset, CK => clk, Q => 
                           x_red(4));
   x_blue_reg_5_inst : dfr11 port map( D => n172, R => reset, CK => clk, Q => 
                           x_blue(5));
   x_red_reg_5_inst : dfr11 port map( D => n171, R => reset, CK => clk, Q => 
                           x_red(5));
   x_blue_reg_6_inst : dfr11 port map( D => n172, R => reset, CK => clk, Q => 
                           x_blue(6));
   x_red_reg_6_inst : dfr11 port map( D => n171, R => reset, CK => clk, Q => 
                           x_red(6));
   U3 : no210 port map( A => reset, B => n13, Y => n14);
   U4 : no210 port map( A => N41, B => N51, Y => n13);
   U5 : no210 port map( A => reset, B => n15, Y => n16);
   U6 : no210 port map( A => N41, B => N52, Y => n15);
   U7 : no210 port map( A => reset, B => n17, Y => n18);
   U12 : no210 port map( A => N41, B => N55, Y => n17);
   U149 : iv110 port map( A => n112, Y => n168);
   U150 : no210 port map( A => reset, B => N41, Y => n112);
   U151 : no210 port map( A => n113, B => quadrant(1), Y => n169);
   U152 : no210 port map( A => n114, B => n113, Y => n170);
   U153 : no310 port map( A => n115, B => N123, C => n116, Y => n113);
   U154 : iv110 port map( A => n117, Y => n116);
   U155 : no310 port map( A => N124, B => N145, C => N125, Y => n117);
   U156 : no210 port map( A => n118, B => quadrant(0), Y => n171);
   U157 : no210 port map( A => n119, B => n118, Y => n172);
   U158 : no310 port map( A => n120, B => N78, C => n121, Y => n118);
   U159 : iv110 port map( A => n122_port, Y => n121);
   U160 : no310 port map( A => N79, B => N97, C => N80, Y => n122_port);
   U161 : mu111 port map( A => N78, B => n123_port, S => quadrant(0), Y => N99)
                           ;
   U162 : mu111 port map( A => N77, B => n120, S => quadrant(0), Y => N98);
   U163 : mu111 port map( A => n124_port, B => N80, S => quadrant(0), Y => N94)
                           ;
   U164 : ex210 port map( A => N79, B => n125_port, Y => N93);
   U165 : no210 port map( A => n126, B => quadrant(0), Y => n125_port);
   U166 : mu111 port map( A => n123_port, B => N78, S => quadrant(0), Y => N92)
                           ;
   U167 : iv110 port map( A => n127, Y => n123_port);
   U168 : ex210 port map( A => N78, B => n128, Y => n127);
   U169 : no210 port map( A => N77, B => N97, Y => n128);
   U170 : mu111 port map( A => n120, B => N77, S => quadrant(0), Y => N91);
   U171 : ex210 port map( A => N77, B => N97, Y => n120);
   U172 : mu111 port map( A => n129, B => n130, S => n131, Y => N60);
   U173 : no210 port map( A => n132, B => n133, Y => n130);
   U174 : na210 port map( A => n134, B => n135, Y => n129);
   U175 : na210 port map( A => n136, B => n133, Y => n135);
   U176 : no310 port map( A => n131, B => step(3), C => n137, Y => N59);
   U177 : na210 port map( A => n138_port, B => n139_port, Y => N58);
   U178 : na210 port map( A => n140_port, B => n141_port, Y => n139_port);
   U179 : na210 port map( A => n142, B => n143, Y => N57);
   U180 : na210 port map( A => n142, B => n144, Y => N56);
   U181 : iv110 port map( A => n145_port, Y => n142);
   U182 : na210 port map( A => n134, B => n146_port, Y => n145_port);
   U183 : na210 port map( A => n147_port, B => n133, Y => n146_port);
   U184 : mu111 port map( A => n148_port, B => n149_port, S => step(1), Y => 
                           n147_port);
   U185 : no210 port map( A => step(3), B => step(2), Y => N55);
   U186 : no210 port map( A => n150, B => n151, Y => N54);
   U187 : no210 port map( A => step(2), B => n148_port, Y => n150);
   U188 : na310 port map( A => n144, B => n134, C => n143, Y => N53);
   U189 : na210 port map( A => n137, B => n149_port, Y => n134);
   U190 : no210 port map( A => n136, B => n133, Y => n137);
   U191 : na210 port map( A => n152, B => n148_port, Y => n144);
   U192 : iv110 port map( A => step(0), Y => n148_port);
   U193 : na210 port map( A => n138_port, B => n153, Y => N52);
   U194 : na210 port map( A => n141_port, B => n131, Y => n153);
   U195 : na210 port map( A => n143, B => n151, Y => N51);
   U196 : iv110 port map( A => n152, Y => n151);
   U197 : no210 port map( A => step(1), B => step(3), Y => n152);
   U198 : na310 port map( A => step(2), B => step(1), C => n154, Y => n143);
   U199 : no210 port map( A => step(3), B => step(0), Y => n154);
   U200 : no210 port map( A => n138_port, B => n131, Y => N41);
   U201 : iv110 port map( A => n140_port, Y => n131);
   U202 : ex210 port map( A => n155, B => step(0), Y => n140_port);
   U203 : iv110 port map( A => step(1), Y => n155);
   U204 : na210 port map( A => n141_port, B => n133, Y => n138_port);
   U205 : iv110 port map( A => step(2), Y => n133);
   U206 : iv110 port map( A => n132, Y => n141_port);
   U207 : na210 port map( A => n149_port, B => n136, Y => n132);
   U208 : na210 port map( A => step(1), B => step(0), Y => n136);
   U209 : iv110 port map( A => step(3), Y => n149_port);
   U210 : mu111 port map( A => N125, B => n156, S => quadrant(1), Y => N149);
   U211 : ex210 port map( A => N124, B => n157, Y => N148);
   U212 : no210 port map( A => n158, B => n114, Y => n157);
   U213 : iv110 port map( A => quadrant(1), Y => n114);
   U214 : mu111 port map( A => N123, B => n159, S => quadrant(1), Y => N147);
   U215 : mu111 port map( A => N122, B => n115, S => quadrant(1), Y => N146);
   U216 : mu111 port map( A => n156, B => N125, S => quadrant(1), Y => N141);
   U217 : ex210 port map( A => n160, B => N125, Y => n156);
   U218 : na210 port map( A => n158, B => n161, Y => n160);
   U219 : iv110 port map( A => N124, Y => n161);
   U220 : ex210 port map( A => N124, B => n162, Y => N140);
   U221 : no210 port map( A => n158, B => quadrant(1), Y => n162);
   U222 : no310 port map( A => N123, B => N145, C => N122, Y => n158);
   U223 : mu111 port map( A => n159, B => N123, S => quadrant(1), Y => N139);
   U224 : iv110 port map( A => n163, Y => n159);
   U225 : ex210 port map( A => N123, B => n164, Y => n163);
   U226 : no210 port map( A => N122, B => N145, Y => n164);
   U227 : mu111 port map( A => n115, B => N122, S => quadrant(1), Y => N138);
   U228 : ex210 port map( A => N122, B => N145, Y => n115);
   U229 : mu111 port map( A => N80, B => n124_port, S => quadrant(0), Y => N101
                           );
   U230 : ex210 port map( A => n165, B => N80, Y => n124_port);
   U231 : na210 port map( A => n126, B => n166, Y => n165);
   U232 : iv110 port map( A => N79, Y => n166);
   U233 : ex210 port map( A => N79, B => n167, Y => N100);
   U234 : no210 port map( A => n126, B => n119, Y => n167);
   U235 : iv110 port map( A => quadrant(0), Y => n119);
   U236 : no310 port map( A => N78, B => N97, C => N77, Y => n126);

end synthesised;



