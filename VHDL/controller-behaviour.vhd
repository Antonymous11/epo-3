library IEEE;
use IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;

architecture behaviour of controller is

type count_bool is (is_counting, not_counting);
type controller_state_type is (reset_state, turning_left, turning_right, pause);
signal counting : count_bool := not_counting;
signal current_counter, next_counter : std_logic_vector(2 downto 0);
signal current_step, next_step : std_logic_vector(3 downto 0);
signal current_quadrant, next_quadrant : std_logic_vector (1 downto 0);
signal current_controller_state : controller_state_type := reset_state;
signal next_controller_state : controller_state_type := reset_state;

begin

step <= current_step;
quadrant <= current_quadrant;

P1: process(clk, reset, enable_reset)
begin
	if (rising_edge(clk)) then
		if (reset = '1' or enable_reset = '1') then
			next_controller_state <= reset_state;
		else
			current_controller_state <= next_controller_state;
			current_step <= next_step;
			current_quadrant <= next_quadrant;
			case current_controller_state is
				when reset_state =>
					next_controller_state <= pause;
				when pause =>
					if ((left = '1' and right = '1') or enable = '1') then
						next_controller_state <= pause;
					elsif (left = '1') then
						next_controller_state <= turning_left;
					elsif (right = '1') then
						next_controller_state <= turning_right;
					else
						next_controller_state <= pause;
					end if;
				when turning_left =>
					if (left = '0' or right = '1' or enable = '1') then
						next_controller_state <= pause;
					else
						next_controller_state <= turning_left;
					end if;
				when turning_right =>
					if (right = '0' or left = '1' or enable = '1') then
						next_controller_state <= pause;
					else
						next_controller_state <= turning_right;
					end if;
			end case;
		end if;
	end if;
end process;

-- Triggers at the 60Hz clock and adds 1 to the counter value if we are turning, or resets it if that is not the case.
P2: process (counter_clk)
begin
	if (rising_edge(counter_clk)) then
		-- When in reset state, set all values to default
		if (current_controller_state = reset_state) then
			next_counter <= "000";
			next_step <= "0000";
			next_quadrant <= "00";
		-- When not in reset, set latch values determined in previous cycle
		else
			current_counter <= next_counter;
			next_quadrant <= current_quadrant;
			next_step <= current_step;
			
			-- If we are in a turning state
			if (current_controller_state = turning_left or current_controller_state = turning_right) then
				-- Reset counter when it is 5, or increment the current value
				if current_counter = "101" then
					next_counter <= (others => '0');
				else
					next_counter <= std_logic_vector(unsigned(current_counter) + 1);
				end if;
				
				-- Activate just when the user presses the button, or the counter has made it's cycle and its time for a new rotation event
				if current_counter = "000" then
					-- Little complicated, but basically a three input xnor gate. Shortest notation to get this functionality
					if (current_quadrant(0) = '1' xnor current_quadrant(1) = '1' xnor current_controller_state = turning_left) then
						-- Previous cycle brought us to the last (8) step, so it's time to invert the first quadrant bit and start counting the other way
						if (current_step = "1000") then
							next_step <= "0111";
							next_quadrant(0) <= not current_quadrant(0);
						-- Just adding one to the step, until it reaches 8 and the condition above is statisfied.
						else
							next_step <= std_logic_vector(unsigned(current_step) + 1);
						end if;
					else
						-- Previous cycle brought us to the first (0) step, so it's time to invert the second quadrant bit and start counting the other way
						if (current_step = "0000") then
							next_step <= "0001";
							next_quadrant(1) <= not current_quadrant(1);
						-- Just subtracting one from the step, until it reaches 0 and the condition above is statisfied.
						else
							next_step <= std_logic_vector(unsigned(current_step) - 1);
						end if;
					end if;
				end if;
				
			-- If not in a turning state, reset the counter
			else
				next_counter <= (others => '0');
			end if;
		end if;
	end if;
end process;

end behaviour;
